import 'jquery';
import 'bootstrap';

global.userLogin = function() {
    var user = $('#user').val();
    abrirSocket(user);
}

function abrirSocket(user){

    $('#chatContainer').show();
    $('#login').hide();

    var divMessage = document.getElementById('chat');
    var user = $('#user').val();
    var socket = io();
    var messagesCounter = 0;

    socket.on('messages', function (msg) {
      alert(msg);
    })

    $( "#m" ).focusin(function() {
      document.title = 'Chat';
      messagesCounter = 0;
    });

    socket.emit('register', user);

    socket.on('usersListConnected', function(userList){
      $('#connected').empty();
      for(var i=0; i<userList.length;i++){
        $('#connected').append($('<div id="'+userList[i].id+'" class="col-md-12">').text(userList[i].user));
      }
    });

    socket.on('alertNewMessage', function(userList){
      messagesCounter++;
      document.title = '('+messagesCounter+')' + ' Nuevos Mensajes';
    });

    $('form').submit(function(){
      socket.emit('chat message', $('#m').val());
      $('#m').val('');
      return false;
    });

    socket.on('chat message', function(msg){
      $('#chat').append($('<div class="col-md-12">').text(msg));
      divMessage.scrollTop = divMessage.scrollHeight;
      //$("#messages").animate({ scrollTop: $('#messages').height()}, 1000);
    });
}
