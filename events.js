var userList = [];

module.exports = function(io){
  io.on('connection', function(socket){
    const _id = socket.id;
    console.log('a user connected');

    socket.on('disconnect', function(){
      //userList = removeUser(_id);
      for(var i=0; i< userList.length; i++){
        if(userList[i].id == _id){
          userList.splice( i, 1);
        }
      }
      io.emit('usersListConnected', userList);
      console.log('user disconnected');
    });

    socket.on('register', function (username) {

      if (username == "" ) {
        socket.emit('messages', 'You are not allow');
      } else {
        userList.push({id:_id,user:username});
        io.emit('usersListConnected', userList);

        //socket.broadcast.emit('userConnected', _id, username);

        socket.on('chat message', function(msg){
          socket.broadcast.emit('alertNewMessage');
          io.emit('chat message', username + ': ' + msg);
        });
      }
    });

  });


}
